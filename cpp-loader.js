'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (src) {
  var cb = this.async();
  return (0, _cPreprocessor.compile)(src, cb);
};

var _cPreprocessor = require('c-preprocessor');

;
module.exports = exports['default'];