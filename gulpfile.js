'use strict';

var gulp = require('gulp'),
    babel = require('gulp-babel'),
    join = require('path').join,
    writeFileSync = require('fs').writeFileSync,
    packageJSON = require('./package'),
    task = gulp.task.bind(gulp),
    tasks = gulp.tasks,
    src = gulp.src.bind(gulp),
    dest = gulp.dest.bind(gulp);

var srcPath = join(__dirname, 'src', '*.js'),
    grabSrcFiles = src.bind(null, srcPath);

var keys = Object.keys;

function forOwn(o, cb, thisArg) {
  keys(o).forEach(function (v) {
    cb.call(thisArg, o[v], v, o);
  });
}

function gulpTask(tsk) {
  return 'node ' + join('node_modules', '.bin', 'gulp') + ' ' + tsk;
}

task('default', ['build']);

task('build', function () {
  return grabSrcFiles()
    .pipe(babel({
      presets: ['es2015', 'stage-2'],
      plugins: ['add-module-exports']
    }))
    .pipe(dest(__dirname));
});

task('build:tasks', function () {
  var scripts = packageJSON.scripts = {};
  forOwn(tasks, function (v, k) {
    scripts[k] = gulpTask(k);
  });
  writeFileSync('./package.json', JSON.stringify(packageJSON, null, 1));
});
