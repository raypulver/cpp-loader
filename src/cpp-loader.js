'use strict';

import { compile } from 'c-preprocessor';

export default function (src) {
  const cb = this.async();
  return compile(src, cb);
};
